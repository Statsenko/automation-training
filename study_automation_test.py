from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import math
import time

# объявляем переменную с URL
link = "http://suninjuly.github.io/explicit_wait2.html"

try:
    # подключаем нужную версию драйвера
    browser = webdriver.Chrome(ChromeDriverManager().install())

    # переходим на страницу по ссылке
    browser.get(link)

    # ждем когда сумма цены станет 100$
    price = WebDriverWait(browser, 12).until(EC.text_to_be_present_in_element((By.ID, "price"), "100"))

    # поле того как сена станет 100$ нажимаем на кнопку Book
    browser.find_element_by_id("book").click()

    # подхватываем значение для вычисления формулы
    x = int(browser.find_element_by_id("input_value").text)

    # находим поле ввода для передачи результата решения формулы
    input1 = browser.find_element_by_id("answer")

    # передаем результат решения формулы
    input1.send_keys(math.log(abs(12*math.sin(x))))

    # нажимаем на кнопку для передачи значения решенного формулой
    browser.find_element_by_id("solve").click()

finally:
    # успеваем скопировать код за 30 секунд
    time.sleep(30)
    # закрываем браузер после всех манипуляций
    browser.quit()
